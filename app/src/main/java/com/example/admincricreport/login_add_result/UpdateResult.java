package com.example.admincricreport.login_add_result;


import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.example.admincricreport.R;
import com.example.admincricreport.helper.LinkHolder;
import com.firebase.client.Firebase;

import java.util.HashMap;


public class UpdateResult extends AppCompatActivity {


    TextView txtkey;
    EditText teamInfo,winTeam,yourTeam;
    String keyHold;
    ProgressBar progressbar;
    Button btnSubmit;

    String key,tmInfo,winteam,yourteam;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_result);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Firebase.setAndroidContext(UpdateResult.this);

        btnSubmit=findViewById(R.id.btnSubmit);

        Intent i=getIntent();
        key=i.getStringExtra("key");
        tmInfo=i.getStringExtra("teamInfo");
        winteam=i.getStringExtra("winTeam");
        yourteam=i.getStringExtra("yourTeam");

//        Toast.makeText(getActivity(), ""+key, Toast.LENGTH_SHORT).show();

        progressbar=findViewById(R.id.progressbar);
        progressbar.setVisibility(View.GONE);
        teamInfo=findViewById(R.id.teamInfo);
        winTeam=findViewById(R.id.winTeam);
        yourTeam=findViewById(R.id.yourTeam);

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                submitClick();
            }
        });

        teamInfo.setText(tmInfo);
        winTeam.setText(winteam);
        yourTeam.setText(yourteam);


    }

    public void submitClick()
    {

        if(teamInfo.getText().toString().isEmpty())
        {
            Toast.makeText(UpdateResult.this, "Enter teams name", Toast.LENGTH_SHORT).show();
        }
        else if(winTeam.getText().toString().isEmpty())
        {
            Toast.makeText(UpdateResult.this, "Enter winning team", Toast.LENGTH_SHORT).show();
        }
        else if(yourTeam.getText().toString().isEmpty())
        {
            Toast.makeText(UpdateResult.this, "Enter your team", Toast.LENGTH_SHORT).show();
        }

        else
        {
            progressbar.setVisibility(View.VISIBLE);
            final Firebase ref=new Firebase(LinkHolder.resultLink);

            final HashMap list=new HashMap();
            list.put("teamInfo",tmInfo);
            list.put("winTeam",winteam);
            list.put("yourTeam",yourteam);


            ref.child(key).updateChildren(list);
                    progressbar.setVisibility(View.GONE);
                    Toast.makeText(UpdateResult.this, "success", Toast.LENGTH_SHORT).show();
                    startActivity(new Intent(UpdateResult.this, Addresult.class));


        }

    }


}