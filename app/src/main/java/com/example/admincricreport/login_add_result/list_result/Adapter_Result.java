package com.example.admincricreport.login_add_result.list_result;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.example.admincricreport.R;
import com.example.admincricreport.login_add_result.UpdateResult;
import com.example.admincricreport.pojo.ResultDetail;


import java.util.ArrayList;
import java.util.List;

public class Adapter_Result extends RecyclerView.Adapter<Adapter_Result.MyViewHolder> {
    private LayoutInflater inflater;
    private static Context context;
    List<Object> data = new ArrayList<>();

    private ProgressDialog dialog;

    public Adapter_Result(Context context, List<Object> data) {
        this.context = context;
        inflater = LayoutInflater.from(context);
        this.data = data;

    }



    @Override
    public Adapter_Result.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());

        View view = layoutInflater.inflate(R.layout.row_pastresult, parent, false);
        Adapter_Result.MyViewHolder viewHolder = new Adapter_Result.MyViewHolder(view);


        return viewHolder;


    }


    @Override
    public void onBindViewHolder(final Adapter_Result.MyViewHolder holder, int position) {

        final ResultDetail current = (ResultDetail) data.get(position);


        holder.mainLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent=new Intent(context, UpdateResult.class);
                intent.putExtra("key",current.getKey());
                intent.putExtra("teamInfo",current.getTeamInfo());
                intent.putExtra("winTeam",current.getWinTeam());
                intent.putExtra("yourTeam",current.getYourTeam());
                context.startActivity(intent);

            }
        });

        holder.txtteamInfo.setText(current.getTeamInfo());
        holder.txtwinTeam.setText(current.getWinTeam());
        holder.txtyourTeam.setText(current.getYourTeam());

        if(current.getWinTeam().equals(current.getYourTeam()))
        {
            holder.imgDoubleClick.setColorFilter(ContextCompat.getColor(context,R.color.green1));
        }
        else
        {

            holder.imgDoubleClick.setColorFilter(ContextCompat.getColor(context,R.color.red));
        }



    }


    @Override
    public int getItemCount() {

        return data.size();
    }


    class MyViewHolder extends RecyclerView.ViewHolder {

        LinearLayout mainLayout;
        ImageView imgDoubleClick;
        TextView txtteamInfo, txtwinTeam, txtyourTeam;

        public MyViewHolder(View itemView) {
            super(itemView);

            mainLayout = itemView.findViewById(R.id.mainLayout);
            imgDoubleClick = itemView.findViewById(R.id.imgdoubleClick);
            txtteamInfo = itemView.findViewById(R.id.txtteamInfo);
            txtwinTeam = itemView.findViewById(R.id.txtwinTeam);
            txtyourTeam = itemView.findViewById(R.id.txtyourTeam);

        }


    }


}

