package com.example.admincricreport.login_add_result;

import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.admincricreport.R;
import com.example.admincricreport.login_add_result.list_result.Adapter_Result;
import com.example.admincricreport.plan_pages.list_plan.Adapter_plans;
import com.example.admincricreport.pojo.ResultDetail;
import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import static com.example.admincricreport.helper.LinkHolder.resultLink;


public class Fragment_allResult extends Fragment {

    View layout;
    Adapter_Result adapter_result;
    RecyclerView recyclerView_upcoming;
    ProgressBar progressBar;
    private boolean firstTime_live = false;

    TextView txtNoUpcomingLive;
    private static Handler handler;
    private static Runnable runnable;

    FirebaseAuth mAuth;
    FirebaseUser user;

    //    plans
    Adapter_plans adapter_plans;
    RecyclerView recyclerView_plan;
    TextView txtNoPlan, txtLivem, txtupcoming, txtrecent;
    public static String match_id_hold;
    public static List<Object> data_upcoming = new ArrayList<>();
    private static List<ResultDetail> listResult = new ArrayList<>();


    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        // Inflate the layout for this fragment
        layout = inflater.inflate(R.layout.fragment_allresult, container, false);

        Firebase.setAndroidContext(getActivity());
        return layout;
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


        recyclerView_upcoming = layout.findViewById(R.id.recycler_upcoming);
        txtNoUpcomingLive = layout.findViewById(R.id.txtNoUpcomingLive);
        progressBar = layout.findViewById(R.id.progressBar);
        mAuth = FirebaseAuth.getInstance();
        user = mAuth.getCurrentUser();

        //        plan
        recyclerView_plan = layout.findViewById(R.id.recycler_plans);
        txtNoPlan = layout.findViewById(R.id.txtNoPlan);
        txtLivem = layout.findViewById(R.id.txtLivem);
        txtupcoming = layout.findViewById(R.id.txtupcoming);
        txtrecent = layout.findViewById(R.id.txtrecent);

        load();



    }


    public void callContinousForTime() {
        if (handler == null) {
            handler = new Handler();
            runnable = new Runnable() {
                @Override
                public void run() {
                    callRecycle_upcoming();
                    handler.postDelayed(runnable, 60000);
                }
            };
            handler.postDelayed(runnable, 1000);
        }

    }

    private void load() {

        try {
            loadFeatured loader = new loadFeatured();
            if (Looper.getMainLooper().getThread() == Thread.currentThread()) {
                Executor executor = Executors.newSingleThreadExecutor();
                executor.execute(loader);
                progressBar.setVisibility(View.VISIBLE);
            }

        } catch (Exception ignored) {
        }

    }

    private class loadFeatured extends Thread {

        @Override
        public void run() {
//            progress.setVisibility(View.GONE);
            android.os.Process.setThreadPriority(android.os.Process.THREAD_PRIORITY_BACKGROUND);

            final Firebase ref = new Firebase(resultLink);

            ref.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                    listResult.clear();


                    if (firstTime_live) {


                        for (DataSnapshot dataSnapshot1 : dataSnapshot.getChildren()) {

                            ResultDetail detail=new ResultDetail();
                            detail.setTeamInfo(dataSnapshot1.child("teamInfo").getValue().toString());
                            detail.setWinTeam(dataSnapshot1.child("winTeam").getValue().toString());
                            detail.setYourTeam(dataSnapshot1.child("yourTeam").getValue().toString());
                            detail.key=dataSnapshot1.getKey();

                            listResult.add(detail);
                        }
                        progressBar.setVisibility(View.GONE);

                    } else {


                        firstTime_live = true;
                        listResult.clear();

                        for (DataSnapshot dataSnapshot1 : dataSnapshot.getChildren()) {

                            ResultDetail detail=new ResultDetail();
                            detail.setTeamInfo(dataSnapshot1.child("teamInfo").getValue().toString());
                            detail.setWinTeam(dataSnapshot1.child("winTeam").getValue().toString());
                            detail.setYourTeam(dataSnapshot1.child("yourTeam").getValue().toString());
                            detail.key=dataSnapshot1.getKey();

                            listResult.add(detail);
                        }

                        progressBar.setVisibility(View.GONE);
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {

                                if (listResult.size() > 0) {
                                    txtNoUpcomingLive.setVisibility(View.GONE);
                                    callRecycle_upcoming();
                                } else {

                                    txtNoUpcomingLive.setVisibility(View.VISIBLE);
//                                    Toast.makeText(getActivity(), "No Upcoming Matches Available", Toast.LENGTH_SHORT).show();
                                }
                            }
                        });

//                        progress.setVisibility(View.GONE);
//                        data_layout.setVisibility(View.VISIBLE);

                    }

                    //ref.removeEventListener(this);

                }

                @Override
                public void onCancelled(FirebaseError firebaseError) {
                    progressBar.setVisibility(View.GONE);
                }
            });


        }
    }

    public void callRecycle_upcoming() {
        adapter_result = new Adapter_Result(getContext(), getData());
        recyclerView_upcoming.setAdapter(adapter_result);
        recyclerView_upcoming.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));

//        ada.onselectClick(new Interface_upcoming_my() {
//            @Override
//            public void onClick(String match_id, int position) {
//                match_id_hold = match_id;

//                Intent intent = new Intent(getActivity(), UpdateMyMatches.class);
//                intent.putExtra("key", match_id);
//                intent.putExtra("positon", position);
//                startActivity(intent);
//            }
//
//        });
    }

    public List<Object> getData() {


        data_upcoming.clear();
        for (int i = 0; i < listResult.size(); i++) {
            ResultDetail info = new ResultDetail();

            info = listResult.get(i);

            data_upcoming.add(info);
        }

        return data_upcoming;
    }

    @Override
    public void onResume() {
        if (firstTime_live) {
            load();
        }
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        firstTime_live = true;
    }


}
