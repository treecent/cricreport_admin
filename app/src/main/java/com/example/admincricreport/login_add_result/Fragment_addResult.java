package com.example.admincricreport.login_add_result;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.example.admincricreport.R;
import com.example.admincricreport.helper.LinkHolder;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;

import java.util.HashMap;


public class Fragment_addResult extends Fragment {

   View layout;
    TextView txtkey;
    EditText teamInfo,winTeam,yourTeam;
    String keyHold;
    ProgressBar progressbar;
    Button btnSubmit;
    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        // Inflate the layout for this fragment
        layout = inflater.inflate(R.layout.fragment_addresult, container, false);

        return layout;
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        Firebase.setAndroidContext(getActivity());

        btnSubmit=layout.findViewById(R.id.btnSubmit);

//        Toast.makeText(getActivity(), ""+key, Toast.LENGTH_SHORT).show();

        progressbar=layout.findViewById(R.id.progressbar);
        progressbar.setVisibility(View.GONE);
        teamInfo=layout.findViewById(R.id.teamInfo);
        winTeam=layout.findViewById(R.id.winTeam);
        yourTeam=layout.findViewById(R.id.yourTeam);

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                submitClick();
            }
        });


    }

    public void submitClick()
    {

        if(teamInfo.getText().toString().isEmpty())
        {
            Toast.makeText(getActivity(), "Enter teams name", Toast.LENGTH_SHORT).show();
        }
        else if(winTeam.getText().toString().isEmpty())
        {
            Toast.makeText(getActivity(), "Enter winning team", Toast.LENGTH_SHORT).show();
        }
        else if(yourTeam.getText().toString().isEmpty())
        {
            Toast.makeText(getActivity(), "Enter your team", Toast.LENGTH_SHORT).show();
        }

        else
        {
            progressbar.setVisibility(View.VISIBLE);
            final Firebase ref=new Firebase(LinkHolder.resultLink);

            final HashMap list=new HashMap();
            list.put("teamInfo",teamInfo.getText().toString());
            list.put("winTeam",winTeam.getText().toString());
            list.put("yourTeam",yourTeam.getText().toString());


            ref.push().setValue(list, new Firebase.CompletionListener() {
                @Override
                public void onComplete(FirebaseError firebaseError, Firebase firebase) {

                    progressbar.setVisibility(View.GONE);
                    Toast.makeText(getActivity(), "success", Toast.LENGTH_SHORT).show();
                    startActivity(new Intent(getActivity(), Addresult.class));

                }
            });

        }

    }

    
}