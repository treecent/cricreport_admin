package com.example.admincricreport.helper;

import androidx.annotation.NonNull;

import com.example.admincricreport.pojo.Featured_fire;
import com.example.admincricreport.pojo.MatchesDetail;

import java.util.ArrayList;
import java.util.List;

public class LinkHolder {

    @NonNull
    public static final String mamaLINK_List = "https://cricket-exchange-6.firebaseio.com/pliveMatches2/";

    public static final ArrayList<Featured_fire> listfeatured_fire = new ArrayList<>();

    public static String premium="https://cricreport-ad8a7-default-rtdb.firebaseio.com/Featured_match_predict";

    public static String resultLink="https://crickhabar-19fb9.firebaseio.com/past_result";

    public static List<MatchesDetail> list_upcoming = new ArrayList<>();
    public static List<MatchesDetail> list_upcoming_hold = new ArrayList<>();
}
