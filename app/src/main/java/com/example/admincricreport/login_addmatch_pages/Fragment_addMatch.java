package com.example.admincricreport.login_addmatch_pages;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.example.admincricreport.R;
import com.example.admincricreport.helper.LinkHolder;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.kunzisoft.switchdatetime.SwitchDateTimeDialogFragment;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Locale;

import static android.content.ContentValues.TAG;


public class Fragment_addMatch extends Fragment {

   View layout;
    TextView txtkey;
    EditText tm1,tm2,series_nm,date,match_no,sts,team1_logo,team2_logo,edtplanPrice,edtplanDesc;
    String keyHold;
    ProgressBar progressbar;
    Button btnSubmit;
    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        // Inflate the layout for this fragment
        layout = inflater.inflate(R.layout.fragment_addmatches, container, false);

        return layout;
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        Firebase.setAndroidContext(getActivity());

        btnSubmit=layout.findViewById(R.id.btnSubmit);

//        Toast.makeText(getActivity(), ""+key, Toast.LENGTH_SHORT).show();

        progressbar=layout.findViewById(R.id.progressbar);
        progressbar.setVisibility(View.GONE);
        tm1=layout.findViewById(R.id.team1);
        tm2=layout.findViewById(R.id.team2);
        series_nm=layout.findViewById(R.id.series);
        date=layout.findViewById(R.id.date);
        match_no=layout.findViewById(R.id.match_no);
        sts=layout.findViewById(R.id.sts);
        team1_logo=layout.findViewById(R.id.flag1);
        team2_logo=layout.findViewById(R.id.flag2);
        edtplanPrice=layout.findViewById(R.id.edtplanPrice);
        edtplanDesc=layout.findViewById(R.id.edtplanDesc);


        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                submitClick();
            }
        });

        date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                timeshow();
            }
        });

//        getDate();

    }

    public void submitClick()
    {

        if(tm1.getText().toString().isEmpty())
        {
            Toast.makeText(getActivity(), "Enter team1 name", Toast.LENGTH_SHORT).show();
        }
        else if(tm2.getText().toString().isEmpty())
        {
            Toast.makeText(getActivity(), "Enter team2 name", Toast.LENGTH_SHORT).show();
        }
        else if(date.getText().toString().isEmpty())
        {
            Toast.makeText(getActivity(), "Enter date", Toast.LENGTH_SHORT).show();
        }
        else if(sts.getText().toString().isEmpty())
        {
            Toast.makeText(getActivity(), "Enter status", Toast.LENGTH_SHORT).show();
        }
        else if(edtplanPrice.getText().toString().isEmpty())
        {
            Toast.makeText(getActivity(), "Enter Plan Price", Toast.LENGTH_SHORT).show();
        }
//        else if(edtplanDesc.getText().toString().isEmpty())
//        {
//            Toast.makeText(getActivity(), "Enter Plan Description", Toast.LENGTH_SHORT).show();
//        }
        else
        {
            progressbar.setVisibility(View.VISIBLE);
            final Firebase ref=new Firebase(LinkHolder.premium);

            final HashMap list=new HashMap();
            list.put("team1",tm1.getText().toString());
            list.put("team2",tm2.getText().toString());
            list.put("series",series_nm.getText().toString());
            list.put("date",date.getText().toString());
            list.put("match_no",match_no.getText().toString());
            list.put("status",sts.getText().toString());
            list.put("flag1",team1_logo.getText().toString());
            list.put("flag2",team2_logo.getText().toString());
            list.put("planPrice",edtplanPrice.getText().toString());
            list.put("planDesc",edtplanDesc.getText().toString());

            ref.push().setValue(list, new Firebase.CompletionListener() {
                @Override
                public void onComplete(FirebaseError firebaseError, Firebase firebase) {

                    progressbar.setVisibility(View.GONE);
                    Toast.makeText(getActivity(), "success", Toast.LENGTH_SHORT).show();
                    startActivity(new Intent(getActivity(), Addmatch.class));

                }
            });

        }

    }

    public void getDate()
    {

        Date currentDate = new Date();

        SimpleDateFormat simpleDate =  new SimpleDateFormat("d MMM, h:mm a");

        String strDt = simpleDate.format(currentDate);
        date.setText(strDt);
    }

    public void timeshow()
    {
        // Initialize
        SwitchDateTimeDialogFragment dateTimeDialogFragment = SwitchDateTimeDialogFragment.newInstance(
                "Set Date Time",
                "OK",
                "Cancel"
        );

// Assign values
        dateTimeDialogFragment.startAtCalendarView();
        dateTimeDialogFragment.set24HoursMode(false);
        dateTimeDialogFragment.setMinimumDateTime(new GregorianCalendar(2018, Calendar.JANUARY, 1).getTime());
        dateTimeDialogFragment.setMaximumDateTime(new GregorianCalendar(2025, Calendar.DECEMBER, 31).getTime());
        dateTimeDialogFragment.setDefaultDateTime(new GregorianCalendar(2021, Calendar.JANUARY, 3, 15, 20).getTime());

// Define new day and month format
        try {
            dateTimeDialogFragment.setSimpleDateMonthAndDayFormat(new SimpleDateFormat("EEE, MMM dd yyyy · hh:mm a", Locale.getDefault()));
        } catch (SwitchDateTimeDialogFragment.SimpleDateMonthAndDayFormatException e) {
            Log.e(TAG, e.getMessage());
        }

// Set listener
        dateTimeDialogFragment.setOnButtonClickListener(new SwitchDateTimeDialogFragment.OnButtonClickListener() {
            @Override
            public void onPositiveButtonClick(Date date_) {
                // Date is get on positive button click
                // Do something
                SimpleDateFormat sdf = new SimpleDateFormat("d MMM, h:mm a");
                String strDt = sdf.format(date_);
                date.setText(strDt);
            }

            @Override
            public void onNegativeButtonClick(Date date) {
                // Date is get on negative button click
            }
        });

// Show
        dateTimeDialogFragment.show(getActivity().getSupportFragmentManager(), "dialog_time");
    }


}