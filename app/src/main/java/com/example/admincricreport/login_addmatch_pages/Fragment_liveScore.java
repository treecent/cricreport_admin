package com.example.admincricreport.login_addmatch_pages;

import android.content.Intent;
import android.os.Bundle;
import android.os.Looper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.example.admincricreport.R;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;


public class Fragment_liveScore extends Fragment {

    View layout;
    TextView txtkey;
    String keyHold,keyPosition,exchange_id;
    private Spinner id_rate;
    private List<String> rate_id_data = new ArrayList<String>();
    private List<String> rate_data = new ArrayList<String>();
    private DatabaseReference ref;

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        // Inflate the layout for this fragment
        layout = inflater.inflate(R.layout.fragment_livescorematches, container, false);

        Firebase.setAndroidContext(getActivity());
        return layout;
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        txtkey=layout.findViewById(R.id.txtkey);

        Intent i=getActivity().getIntent();
        keyHold=i.getStringExtra("key");
        keyPosition=i.getStringExtra("positon");

        txtkey.setText("key :"+keyHold);

        FirebaseDatabase database = FirebaseDatabase.getInstance();
        ref = database.getReference("Livescore/" + keyHold);


        id_rate = (Spinner) view.findViewById(R.id.id_rate);

        id_loader id_loader = new id_loader();
        if (Looper.getMainLooper().getThread() == Thread.currentThread()) {
            Executor executor = Executors.newSingleThreadExecutor();
            executor.execute(id_loader);
        }


        id_rate.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                if (!id_rate.getSelectedItem().toString().matches("-")) {

                    ref.child("exchange_id").setValue(String.valueOf(rate_id_data.get(id_rate.getSelectedItemPosition())));

                }
                else
                {
                    ref.child("exchange_id").setValue("none");
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

    }


    public class id_loader extends Thread {

        @Override
        public void run() {

            android.os.Process.setThreadPriority(android.os.Process.THREAD_PRIORITY_BACKGROUND);

            Firebase.setAndroidContext(getActivity());

            final Firebase ref_client = new Firebase("https://cricket-exchange-6.firebaseio.com/pliveMatches2");

            rate_data.add("-");
            rate_id_data.add("-");

            ref_client.addValueEventListener(new com.firebase.client.ValueEventListener() {
                @Override
                public void onDataChange(com.firebase.client.DataSnapshot dataSnapshot) {

                    for (com.firebase.client.DataSnapshot dataSnapshot1 : dataSnapshot.getChildren()) {
                        rate_data.add(String.valueOf(dataSnapshot1.child("j").getValue()) + ", " + String.valueOf(dataSnapshot1.child("k").getValue()));
                        rate_id_data.add(String.valueOf(dataSnapshot1.getKey()));
                    }
                    ArrayAdapter<String> dataAdapter=null;
                    try {
                         dataAdapter = new ArrayAdapter<String>(getActivity().getApplicationContext(), android.R.layout.simple_spinner_item, rate_data);
                        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                    }catch (Exception e1)
                    {

                    }

                    id_rate.setAdapter(dataAdapter);

                    try {

                        for (int i = 0; i < rate_id_data.size(); i++) {
                            if (rate_id_data.get(i).matches(exchange_id)) {
                                id_rate.setSelection(i);
                            }
                        }

                    } catch (Exception e) {
                        Toast.makeText(getActivity(), "Select Match Id To Live Score", Toast.LENGTH_SHORT).show();
                    }

                    ref_client.removeEventListener(this);

                }

                @Override
                public void onCancelled(FirebaseError firebaseError) {

                }
            });

            id_rate.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                    if (!id_rate.getSelectedItem().toString().matches("-")) {

                        ref.child("mamaID").setValue(String.valueOf(rate_id_data.get(id_rate.getSelectedItemPosition())));

                    }

                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });

        }
    }


}
