package com.example.admincricreport.login_addmatch_pages;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.admincricreport.R;
import com.example.admincricreport.fragment_matches.upcoming.Adapter_upcoming_mycontest;
import com.example.admincricreport.fragment_matches.upcoming.Interface_upcoming_my;

import com.example.admincricreport.plan_pages.list_plan.Adapter_plans;
import com.example.admincricreport.pojo.MatchesDetail;
import com.example.admincricreport.pojo.PlansDetail;
import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import static com.example.admincricreport.helper.LinkHolder.list_upcoming;
import static com.example.admincricreport.helper.LinkHolder.list_upcoming_hold;
import static com.example.admincricreport.helper.LinkHolder.premium;


public class Fragment_myMatch extends Fragment {

    View layout;
    Adapter_upcoming_mycontest adapter_upcoming;
    RecyclerView recyclerView_upcoming;
    ProgressBar progressBar, progressbar_plan;
    private boolean firstTime_live = false;

    TextView txtNoUpcomingLive;
    private static Handler handler;
    private static Runnable runnable;
    LinearLayout popuplayout;
    Button btnBack;

    private static ArrayList<String> listkey = new ArrayList<>();
    private static ArrayList<String> plankeys = new ArrayList<>();
    FirebaseAuth mAuth;
    FirebaseUser user;

    //    plans
    Adapter_plans adapter_plans;
    RecyclerView recyclerView_plan;
    private static List<PlansDetail> listPlans = new ArrayList<>();
    TextView txtNoPlan, txtLivem, txtupcoming, txtrecent;
    public static String match_id_hold;
    public static List<Object> data_upcoming = new ArrayList<>();


    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        // Inflate the layout for this fragment
        layout = inflater.inflate(R.layout.fragment_myallmatches, container, false);

        Firebase.setAndroidContext(getActivity());
        return layout;
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


        recyclerView_upcoming = layout.findViewById(R.id.recycler_upcoming);
        txtNoUpcomingLive = layout.findViewById(R.id.txtNoUpcomingLive);
        progressBar = layout.findViewById(R.id.progressBar);
        mAuth = FirebaseAuth.getInstance();
        user = mAuth.getCurrentUser();

        //        plan
        recyclerView_plan = layout.findViewById(R.id.recycler_plans);
        txtNoPlan = layout.findViewById(R.id.txtNoPlan);
        txtLivem = layout.findViewById(R.id.txtLivem);
        txtupcoming = layout.findViewById(R.id.txtupcoming);
        txtrecent = layout.findViewById(R.id.txtrecent);


        load();

        txtLivem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                List<MatchesDetail> list_temp = new ArrayList<>();
                list_temp.clear();
                for (int i = 0; i < list_upcoming_hold.size(); i++) {
                    if (list_upcoming_hold.get(i).live.equals("1")) {
                        list_temp.add(list_upcoming_hold.get(i));
                    }

                }


                list_upcoming.clear();
                list_upcoming = list_temp;
                getUpcomingData();
                adapter_upcoming.notifyDataSetChanged();

                txtLivem.setBackgroundColor(getResources().getColor(R.color.red));
                txtupcoming.setBackgroundColor(getResources().getColor(R.color.yellow_faint));
                txtrecent.setBackgroundColor(getResources().getColor(R.color.yellow_faint));


            }
        });

        txtupcoming.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                List<MatchesDetail> list_temp = new ArrayList<>();
                list_temp.clear();
                for (int i = 0; i < list_upcoming_hold.size(); i++) {
                    if (list_upcoming_hold.get(i).live.equals("0")) {
                        list_temp.add(list_upcoming_hold.get(i));
                    }

                }


                list_upcoming.clear();
                list_upcoming = list_temp;
                getUpcomingData();
                adapter_upcoming.notifyDataSetChanged();

                txtupcoming.setBackgroundColor(getResources().getColor(R.color.red));
                txtLivem.setBackgroundColor(getResources().getColor(R.color.yellow_faint));
                txtrecent.setBackgroundColor(getResources().getColor(R.color.yellow_faint));

            }
        });

        txtrecent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                List<MatchesDetail> list_temp = new ArrayList<>();
                list_temp.clear();
                for (int i = 0; i < list_upcoming_hold.size(); i++) {
                    if (list_upcoming_hold.get(i).live.equals("2")) {
                        list_temp.add(list_upcoming_hold.get(i));
                    }

                }

                list_upcoming.clear();
                list_upcoming = list_temp;
                getUpcomingData();
                adapter_upcoming.notifyDataSetChanged();

                txtLivem.setBackgroundColor(getResources().getColor(R.color.yellow_faint));
                txtupcoming.setBackgroundColor(getResources().getColor(R.color.yellow_faint));
                txtrecent.setBackgroundColor(getResources().getColor(R.color.red));


            }
        });


    }


    public void callContinousForTime() {
        if (handler == null) {
            handler = new Handler();
            runnable = new Runnable() {
                @Override
                public void run() {
                    callRecycle_upcoming();
                    handler.postDelayed(runnable, 60000);
                }
            };
            handler.postDelayed(runnable, 1000);
        }

    }

    private void load() {

        try {
            loadFeatured loader = new loadFeatured();
            if (Looper.getMainLooper().getThread() == Thread.currentThread()) {
                Executor executor = Executors.newSingleThreadExecutor();
                executor.execute(loader);
                progressBar.setVisibility(View.VISIBLE);
            }

        } catch (Exception ignored) {
        }

    }

    private class loadFeatured extends Thread {

        @Override
        public void run() {
//            progress.setVisibility(View.GONE);
            android.os.Process.setThreadPriority(android.os.Process.THREAD_PRIORITY_BACKGROUND);

            final Firebase ref = new Firebase(premium);

            ref.orderByChild("date").addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                    list_upcoming.clear();


                    if (firstTime_live) {


                        for (DataSnapshot dataSnapshot1 : dataSnapshot.getChildren()) {

                            MatchesDetail listdata = new MatchesDetail();

                            listdata.team_1_logo = String.valueOf(dataSnapshot1.child("flag1").getValue());
                            listdata.team_2_logo = String.valueOf(dataSnapshot1.child("flag2").getValue());
                            listdata.nickname_1 = String.valueOf(dataSnapshot1.child("team1").getValue());
                            listdata.nickname_2 = String.valueOf(dataSnapshot1.child("team2").getValue());
                            listdata.match_id = String.valueOf(dataSnapshot1.getKey());
                            listdata.live = String.valueOf(dataSnapshot1.child("status").getValue());
                            listdata.series = String.valueOf(dataSnapshot1.child("series").getValue());
                            listdata.active = String.valueOf(dataSnapshot1.child("active").getValue());
                            listdata.date_time = String.valueOf(dataSnapshot1.child("date").getValue());
                            listdata.match_no = String.valueOf(dataSnapshot1.child("match_no").getValue());
                            listdata.planPrice = String.valueOf(dataSnapshot1.child("planPrice").getValue());
                            listdata.planDesc = String.valueOf(dataSnapshot1.child("planDesc").getValue());

                            list_upcoming.add(listdata);

                        }
                         list_upcoming_hold.clear();
                        for (int i = 0; i < list_upcoming.size(); i++) {
                            list_upcoming_hold.add(list_upcoming.get(i));
                        }
                        getUpcomingData();
                        try {
                            adapter_upcoming.notifyDataSetChanged();
                        }catch (Exception e1)
                        {}

                        progressBar.setVisibility(View.GONE);

                    } else {


                        firstTime_live = true;
                        list_upcoming.clear();

                        for (DataSnapshot dataSnapshot1 : dataSnapshot.getChildren()) {

                            MatchesDetail listdata = new MatchesDetail();

                            listdata.fav = "";
                            listdata.rate_1 = "";
                            listdata.rate_2 = "";
                            listdata.team_1_logo = String.valueOf(dataSnapshot1.child("flag1").getValue());
                            listdata.team_2_logo = String.valueOf(dataSnapshot1.child("flag2").getValue());
                            listdata.nickname_1 = String.valueOf(dataSnapshot1.child("team1").getValue());
                            listdata.nickname_2 = String.valueOf(dataSnapshot1.child("team2").getValue());
                            listdata.match_id = String.valueOf(dataSnapshot1.getKey());
                            listdata.live = String.valueOf(dataSnapshot1.child("status").getValue());
                            listdata.series = String.valueOf(dataSnapshot1.child("series").getValue());
                            listdata.target = "";
                            listdata.extra_text = "";
                            listdata.date_time = String.valueOf(dataSnapshot1.child("date").getValue());
                            listdata.match_no = "";

                            list_upcoming.add(listdata);

                        }

                        list_upcoming_hold.clear();
                        for (int i = 0; i < list_upcoming.size(); i++) {
                            list_upcoming_hold.add(list_upcoming.get(i));
                        }
                        progressBar.setVisibility(View.GONE);
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {

                                if (list_upcoming.size() > 0) {
                                    txtNoUpcomingLive.setVisibility(View.GONE);
                                    callRecycle_upcoming();
                                } else {

                                    txtNoUpcomingLive.setVisibility(View.VISIBLE);
//                                    Toast.makeText(getActivity(), "No Upcoming Matches Available", Toast.LENGTH_SHORT).show();
                                }
                            }
                        });

//                        progress.setVisibility(View.GONE);
//                        data_layout.setVisibility(View.VISIBLE);

                    }

                    //ref.removeEventListener(this);

                }

                @Override
                public void onCancelled(FirebaseError firebaseError) {
                    progressBar.setVisibility(View.GONE);
                }
            });


        }
    }

    public void callRecycle_upcoming() {
        adapter_upcoming = new Adapter_upcoming_mycontest(getContext(), getUpcomingData());
        recyclerView_upcoming.setAdapter(adapter_upcoming);
        recyclerView_upcoming.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));

        adapter_upcoming.onselectClick(new Interface_upcoming_my() {
            @Override
            public void onClick(String match_id, int position) {
                match_id_hold = match_id;

                Intent intent = new Intent(getActivity(), UpdateMyMatches.class);
                intent.putExtra("key", match_id);
                intent.putExtra("positon", position);
                startActivity(intent);
            }

        });
    }

    public List<Object> getUpcomingData() {


        data_upcoming.clear();
        for (int i = 0; i < list_upcoming.size(); i++) {
            MatchesDetail info = new MatchesDetail();

            info = list_upcoming.get(i);

            data_upcoming.add(info);
        }

        return data_upcoming;
    }

    @Override
    public void onResume() {
        if (firstTime_live) {
            load();
        }
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        firstTime_live = true;
    }


}
