package com.example.admincricreport;

import android.content.Intent;
import android.os.Bundle;

import com.example.admincricreport.login_add_result.Addresult;
import com.example.admincricreport.login_addmatch_pages.Addmatch;
import com.example.admincricreport.login_detail_page.Detail_page;
import com.example.admincricreport.login_plan_pages.Login_plans;

import androidx.appcompat.app.AppCompatActivity;

import android.view.View;
import android.widget.Button;

public class HomePage extends AppCompatActivity {


    Button btnPlans,btnDetail,btnMatch;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        btnDetail=findViewById(R.id.btndetail);
        btnMatch=findViewById(R.id.btnmatch);
        btnPlans=findViewById(R.id.btnplan);
    }

    public void  btnClick(View v)
    {
        if(v.getId()==R.id.btnplan)
        {
            startActivity(new Intent(HomePage.this, Login_plans.class));
        }
        else if(v.getId()==R.id.btndetail)
        {
            startActivity(new Intent(HomePage.this, Detail_page.class));
        }
        else if(v.getId()==R.id.btnmatch)
        {
            startActivity(new Intent(HomePage.this, Addmatch.class));
        }
        else if(v.getId()==R.id.btnresult)
        {
            startActivity(new Intent(HomePage.this, Addresult.class));
        }
    }


}
