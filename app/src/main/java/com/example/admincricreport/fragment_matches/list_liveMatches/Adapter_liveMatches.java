package com.example.admincricreport.fragment_matches.list_liveMatches;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.admincricreport.R;
import com.example.admincricreport.pojo.MatchesDetail;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class Adapter_liveMatches extends RecyclerView.Adapter<Adapter_liveMatches.MyViewHolder> {
    private LayoutInflater inflater;
    private static Context context;
    private static Activity activity;
    Interface_live_my interface_live_my;

    List<Object> data = new ArrayList<>();

    private ProgressDialog dialog;

    public Adapter_liveMatches(Context context, Activity activity, List<Object> data) {
        this.context = context;
        inflater = LayoutInflater.from(context);
        this.data = data;
        this.activity = activity;

    }

    public void onselectClick(Interface_live_my interface_live_my)
    {
        this.interface_live_my=interface_live_my;
    }


    @Override
    public Adapter_liveMatches.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());

        View view = layoutInflater.inflate(R.layout.row_live_my, parent, false);
        Adapter_liveMatches.MyViewHolder viewHolder = new Adapter_liveMatches.MyViewHolder(view);


        return viewHolder;


    }


    @Override
    public void onBindViewHolder(final Adapter_liveMatches.MyViewHolder holder, int position) {

        final MatchesDetail current = (MatchesDetail) data.get(position);


        holder.mainLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//                Intent intent = new Intent(context, LiveContest.class);
//                intent.putExtra("mid", current.match_id);
//                context.startActivity(intent);
                interface_live_my.onClick(current.match_id);
            }
        });

//        if(position%2==0)
//        {
//            holder.cardView.setCardBackgroundColor(ContextCompat.getColor(context,R.color.cback1));
//            holder.txtplan.setBackground(ContextCompat.getDrawable(context,R.drawable.button_back_1));
//        }
//        else if(position%3==0)
//        {
//            holder.cardView.setCardBackgroundColor(ContextCompat.getColor(context,R.color.cback2));
//            holder.txtplan.setBackground(ContextCompat.getDrawable(context,R.drawable.button_back_2));
//        }
//        else
//        {
//            holder.cardView.setCardBackgroundColor(ContextCompat.getColor(context,R.color.cback2));
//            holder.txtplan.setBackground(ContextCompat.getDrawable(context,R.drawable.button_back_2));
//        }
        Glide.with(context).load(current.team_1_logo).placeholder(R.mipmap.ic_launcher).into(holder.imgTeam1_logo);
        Glide.with(context).load(current.team_2_logo).placeholder(R.mipmap.ic_launcher).into(holder.imgTeam2_logo);

        holder.txtTeam1ShortName.setText(current.nickname_1);
        holder.txtTeam2ShortName.setText(current.nickname_2);
        holder.txtSeriesName.setText(current.series);


    }


    @Override
    public int getItemCount() {

        return data.size();
    }


    class MyViewHolder extends RecyclerView.ViewHolder {

        LinearLayout mainLayout;
        CircleImageView imgTeam1_logo, imgTeam2_logo;
        TextView txtTeam1Name, txtTeam2Name, txtTeam1ShortName, txtTeam2ShortName, txtRemainTime, txtSeriesName,txtplan;
        CardView cardView;

        public MyViewHolder(View itemView) {
            super(itemView);

            mainLayout = itemView.findViewById(R.id.mainLayout);
            imgTeam1_logo = itemView.findViewById(R.id.imgTeam1_logo);
            imgTeam2_logo = itemView.findViewById(R.id.imgTeam2_logo);

            txtTeam1ShortName = itemView.findViewById(R.id.txtTeam1ShortName);
            txtTeam2ShortName = itemView.findViewById(R.id.txtTeam2ShortName);

            txtSeriesName = itemView.findViewById(R.id.txtSeriesName);
            txtRemainTime = itemView.findViewById(R.id.txtRemainTime);
            txtplan = itemView.findViewById(R.id.txtplan);
            cardView= itemView.findViewById(R.id.cardView);


        }


    }


}

