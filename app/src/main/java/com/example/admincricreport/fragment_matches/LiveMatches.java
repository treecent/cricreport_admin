package com.example.admincricreport.fragment_matches;

import android.content.Intent;
import android.os.Bundle;
import android.os.Looper;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.admincricreport.R;
import com.example.admincricreport.fragment_matches.list_liveMatches.Adapter_liveMatches;
import com.example.admincricreport.fragment_matches.list_liveMatches.Interface_live_my;
import com.example.admincricreport.helper.LinkHolder;
import com.example.admincricreport.plan_pages.Plans;
import com.example.admincricreport.plan_pages.list_plan.Adapter_plans;
import com.example.admincricreport.plan_pages.list_plan.Interface_plans;
import com.example.admincricreport.pojo.MatchesDetail;
import com.example.admincricreport.pojo.PlansDetail;
import com.example.admincricreport.pojo.PlansKeyDetail;
import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.orhanobut.dialogplus.DialogPlus;
import com.orhanobut.dialogplus.OnDismissListener;
import com.orhanobut.dialogplus.OnItemClickListener;
import com.orhanobut.dialogplus.ViewHolder;


import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;


public class LiveMatches extends Fragment implements View.OnClickListener {


    View layout;

    Adapter_liveMatches adapter_live;
    RecyclerView recyclerView_live;
    TextView txtNoLive;
    ProgressBar progressBar,progressbar_plan;
    private boolean firstTime_live = false;
    private final List<MatchesDetail> list_live = new ArrayList<>();
    private static ArrayList<String> listkey = new ArrayList<>();
    private static ArrayList<String> plankeys = new ArrayList<>();
    FirebaseAuth mAuth;
    FirebaseUser user;
    //    plans
    Adapter_plans adapter_plans;
    RecyclerView recyclerView_plan;
    private static List<PlansDetail> listPlans = new ArrayList<>();
    TextView txtNoPlan;
    public static String match_id_hold;

    LinearLayout popuplayout;
    Button btnBack;


    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        // Inflate the layout for this fragment
        layout = inflater.inflate(R.layout.fragment_livematches, container, false);
        return layout;
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        recyclerView_live = layout.findViewById(R.id.recycler_live);
        txtNoLive = layout.findViewById(R.id.txtNoLive);
        txtNoLive.setVisibility(View.GONE);

        progressBar = layout.findViewById(R.id.progressBar);

        popuplayout= layout.findViewById(R.id.popuplayout);
        btnBack = layout.findViewById(R.id.btnBackpop);

        mAuth = FirebaseAuth.getInstance();
        user = mAuth.getCurrentUser();

        //        plan
        recyclerView_plan = layout.findViewById(R.id.recycler_plans);
        txtNoPlan = layout.findViewById(R.id.txtNoPlan);

        progressbar_plan = layout.findViewById(R.id.progressBar_plan);
        progressbar_plan.setVisibility(View.GONE);


        load();

    }


    private void load() {

        try {
            loadFeatured loader = new loadFeatured();
            if (Looper.getMainLooper().getThread() == Thread.currentThread()) {
                Executor executor = Executors.newSingleThreadExecutor();
                executor.execute(loader);

            }

        } catch (Exception ignored) {
        }

    }

    private class loadFeatured extends Thread {

        @Override
        public void run() {
//            progress.setVisibility(View.GONE);
            android.os.Process.setThreadPriority(android.os.Process.THREAD_PRIORITY_BACKGROUND);

            final Firebase ref = new Firebase(LinkHolder.premium);

            ref.orderByChild("date").addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                    list_live.clear();

                    if (firstTime_live) {


                        for (DataSnapshot dataSnapshot1 : dataSnapshot.getChildren()) {

                            MatchesDetail listdata = new MatchesDetail();

                            listdata.fav = "";
                            listdata.rate_1 = "";
                            listdata.rate_2 = "";
                            listdata.team_1_logo = String.valueOf(dataSnapshot1.child("flag1").getValue());
                            listdata.team_2_logo = String.valueOf(dataSnapshot1.child("flag2").getValue());
                            listdata.nickname_1 = String.valueOf(dataSnapshot1.child("team1").getValue());
                            listdata.nickname_2 = String.valueOf(dataSnapshot1.child("team2").getValue());
                            listdata.match_id = String.valueOf(dataSnapshot1.getKey());
                            listdata.live = String.valueOf(dataSnapshot1.child("status").getValue());
                            listdata.series = String.valueOf(dataSnapshot1.child("series").getValue());
                            listdata.target = "";
                            listdata.extra_text = "";
                            listdata.date_time = String.valueOf(dataSnapshot1.child("date").getValue());
                            listdata.match_no = "";

                            if (String.valueOf(dataSnapshot1.child("status").getValue()).equals("1")) {
                                list_live.add(listdata);
                            } else {

                            }

                        }



                    } else {


                        firstTime_live = true;

                        for (DataSnapshot dataSnapshot1 : dataSnapshot.getChildren()) {

                            MatchesDetail listdata = new MatchesDetail();

                            listdata.fav = "";
                            listdata.rate_1 = "";
                            listdata.rate_2 = "";
                            listdata.team_1_logo = String.valueOf(dataSnapshot1.child("flag1").getValue());
                            listdata.team_2_logo = String.valueOf(dataSnapshot1.child("flag2").getValue());
                            listdata.nickname_1 = String.valueOf(dataSnapshot1.child("team1").getValue());
                            listdata.nickname_2 = String.valueOf(dataSnapshot1.child("team2").getValue());
                            listdata.match_id = String.valueOf(dataSnapshot1.getKey());
                            listdata.live = String.valueOf(dataSnapshot1.child("status").getValue());
                            listdata.series = String.valueOf(dataSnapshot1.child("series").getValue());
                            listdata.target = "";
                            listdata.extra_text = "";
                            listdata.date_time = String.valueOf(dataSnapshot1.child("date").getValue());
                            listdata.match_no = "";

                            if (String.valueOf(dataSnapshot1.child("status").getValue()).equals("1")) {
                                list_live.add(listdata);
                            } else {

                            }

                        }
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {

                                if (list_live.size() > 0) {
                                    callRecycle_recent();
                                    progressBar.setVisibility(View.GONE);
                                } else {
//                                    Toast.makeText(getActivity(), "No Live Matches Available", Toast.LENGTH_SHORT).show();
                                    txtNoLive.setVisibility(View.VISIBLE);
                                    progressBar.setVisibility(View.GONE);
                                }
                            }
                        });

//                        progress.setVisibility(View.GONE);
//                        data_layout.setVisibility(View.VISIBLE);

                    }

                    //ref.removeEventListener(this);

                }

                @Override
                public void onCancelled(FirebaseError firebaseError) {
                    progressBar.setVisibility(View.GONE);
                }
            });


        }
    }

    public void callRecycle_recent() {
        adapter_live = new Adapter_liveMatches(getContext(), getActivity(), getUpcomingData());
        recyclerView_live.setAdapter(adapter_live);
        recyclerView_live.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));

        adapter_live.onselectClick(new Interface_live_my() {
            @Override
            public void onClick(String match_id) {
                match_id_hold=match_id;
                Intent intent =new Intent(getActivity(), Plans.class);
                intent.putExtra("match_id",match_id_hold);
                startActivity(intent);
            }
        });
    }

    public List<Object> getUpcomingData() {
        List<Object> data_upcoming = new ArrayList<>();

        for (int i = 0; i < list_live.size(); i++) {
            MatchesDetail info = new MatchesDetail();

            info = list_live.get(i);

            data_upcoming.add(info);
        }

        return data_upcoming;
    }

    @Override
    public void onResume() {
        if (firstTime_live) {
            load();
        }
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        firstTime_live = true;
    }


    @Override
    public void onClick(View v) {

    }

    public void showPopup() {

        popuplayout.setVisibility(View.VISIBLE);

        final DialogPlus dialog = DialogPlus.newDialog(getActivity())


                .setContentHolder(new ViewHolder(popuplayout))
                .setOnItemClickListener(new OnItemClickListener() {
                    @Override
                    public void onItemClick(DialogPlus dialog, Object item, View view, int position) {
                    }
                })
                .setExpanded(true, ViewGroup.LayoutParams.WRAP_CONTENT)
                .setGravity(Gravity.BOTTOM)
                .setMargin(0, 500, 0, 0)
//                .setInAnimation(R.anim.abc_fade_in)
//                .setOutAnimation(R.anim.abc_fade_out)


                .setContentBackgroundResource(R.color.colorPrimary_faint)// This will enable the expand feature, (similar to android L share dialog)
                .setCancelable(true)

                .setOnDismissListener(new OnDismissListener() {
                    @Override
                    public void onDismiss(DialogPlus dialogPlus) {
                        popuplayout.setVisibility(View.GONE);
                    }
                })


                .create();

        dialog.show();


        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

    }


    public void load_plan() {
        try {
           LoadPlan loader = new LoadPlan();
            if (Looper.getMainLooper().getThread() == Thread.currentThread()) {
                Executor executor = Executors.newSingleThreadExecutor();
                executor.execute(loader);
                progressbar_plan.setVisibility(View.VISIBLE);
            }

        } catch (Exception ignored) {
        }

    }


    private class LoadPlan extends Thread {
        @Override
        public void run() {
            android.os.Process.setThreadPriority(android.os.Process.THREAD_PRIORITY_BACKGROUND);

            FirebaseDatabase database = FirebaseDatabase.getInstance();

            final DatabaseReference ref = database.getReference("/Plans");

            ref.orderByChild("listing").addValueEventListener(new com.google.firebase.database.ValueEventListener() {
                @Override
                public void onDataChange(@NonNull com.google.firebase.database.DataSnapshot snapshot) {


                    listPlans.clear();
                    final PlansDetail current = new PlansDetail();


                    for (final com.google.firebase.database.DataSnapshot dataSnapshot : snapshot.getChildren()) {
                        final PlansKeyDetail detail = new PlansKeyDetail();

                        if ((dataSnapshot.child("status").getValue().equals("1"))) {

                            detail.key = String.valueOf(dataSnapshot.getKey());
                            detail.status = String.valueOf(dataSnapshot.child("status").getValue());
                            detail.poolprice = String.valueOf(dataSnapshot.child("poolprice").getValue());
                            current.listplan.add(detail);

                        }

                    }


                    listPlans.add(current);
                    progressbar_plan.setVisibility(View.GONE);
                    if (listPlans != null) {
                        if (listPlans.size() > 0) {
                            callrecycle();
                        }
                    }
                    if (listPlans.get(0).listplan.size() > 0) {

                    } else {
                        txtNoPlan.setVisibility(View.VISIBLE);
                    }


                }

                @Override
                public void onCancelled(@NonNull DatabaseError error) {
                    progressbar_plan.setVisibility(View.GONE);
                    Toast.makeText(getActivity(), "" + error.getMessage(), Toast.LENGTH_SHORT).show();
                }
            });


        }
    }


    public void callrecycle() {
        adapter_plans = new Adapter_plans(getActivity(), getData(),match_id_hold);
        recyclerView_plan.setAdapter(adapter_plans);
        recyclerView_plan.setLayoutManager(new LinearLayoutManager(getActivity(), RecyclerView.VERTICAL, false));

        adapter_plans.onselectClick(new Interface_plans() {
            @Override
            public void onClick(int Position) {


            }
        });
    }

    public List<Object> getData() {
        List<Object> data = new ArrayList<>();

        for (int i = 0; i < listPlans.size(); i++) {
            PlansDetail detail = new PlansDetail();

            detail = listPlans.get(i);

            data.add(detail);


        }
        return data;
    }





}
