package com.example.admincricreport.pojo;

public class PlansKeyDetail {

    public String totalspots;
    public String title;
    public String subtitle;
    public String status;
    public String poolprice;
    public String match_id;
    public String inning;
    public String entryprice;
    public String date;
    public String category;
    public String assignspot;

    public String getPlantype() {
        return plantype;
    }

    public void setPlantype(String plantype) {
        this.plantype = plantype;
    }

    public String plantype;

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String desc;

    public String getListing() {
        return listing;
    }

    public void setListing(String listing) {
        this.listing = listing;
    }

    public String listing;


    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String key;

    public String getFirstprice() {
        return firstprice;
    }

    public void setFirstprice(String firstprice) {
        this.firstprice = firstprice;
    }

    public String firstprice;

    public String getTotalspots() {
        return totalspots;
    }

    public void setTotalspots(String totalspots) {
        this.totalspots = totalspots;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSubtitle() {
        return subtitle;
    }

    public void setSubtitle(String subtitle) {
        this.subtitle = subtitle;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getPoolprice() {
        return poolprice;
    }

    public void setPoolprice(String poolprice) {
        this.poolprice = poolprice;
    }

    public String getMatch_id() {
        return match_id;
    }

    public void setMatch_id(String match_id) {
        this.match_id = match_id;
    }

    public String getInning() {
        return inning;
    }

    public void setInning(String inning) {
        this.inning = inning;
    }

    public String getEntryprice() {
        return entryprice;
    }

    public void setEntryprice(String entryprice) {
        this.entryprice = entryprice;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getAssignspot() {
        return assignspot;
    }

    public void setAssignspot(String assignspot) {
        this.assignspot = assignspot;
    }
}
