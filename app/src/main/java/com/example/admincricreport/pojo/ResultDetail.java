package com.example.admincricreport.pojo;

public class ResultDetail {

    public String teamInfo;
    public String winTeam;
    public String yourTeam;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String key;

    public String getTeamInfo() {
        return teamInfo;
    }

    public void setTeamInfo(String teamInfo) {
        this.teamInfo = teamInfo;
    }

    public String getWinTeam() {
        return winTeam;
    }

    public void setWinTeam(String winTeam) {
        this.winTeam = winTeam;
    }

    public String getYourTeam() {
        return yourTeam;
    }

    public void setYourTeam(String yourTeam) {
        this.yourTeam = yourTeam;
    }
}
