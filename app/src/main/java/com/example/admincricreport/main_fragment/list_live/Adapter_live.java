package com.example.admincricreport.main_fragment.list_live;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.admincricreport.R;
import com.example.admincricreport.pojo.MatchesDetail;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class Adapter_live extends RecyclerView.Adapter<Adapter_live.MyViewHolder> {
    private LayoutInflater inflater;
    private static Context context;
    private static Activity activity;
    public Interface_live interface_live;

    List<Object> data = new ArrayList<>();

    private ProgressDialog dialog;

    public Adapter_live(Context context, Activity activity, List<Object> data) {
        this.context = context;
        inflater = LayoutInflater.from(context);
        this.data = data;
        this.activity = activity;

    }

    public void onselectClick(Interface_live interface_live)
    {
        this.interface_live=interface_live;
    }


    @Override
    public Adapter_live.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());

        View view = layoutInflater.inflate(R.layout.row_live, parent, false);
        Adapter_live.MyViewHolder viewHolder = new Adapter_live.MyViewHolder(view);


        return viewHolder;


    }


    @Override
    public void onBindViewHolder(final Adapter_live.MyViewHolder holder, int position) {

        final MatchesDetail current = (MatchesDetail) data.get(position);


        holder.mainLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                interface_live.onClick(holder.getAdapterPosition());
//                Intent intent = new Intent(context, Plans.class);
//                intent.putExtra("mid", current.match_id);
//                intent.putExtra("m_type", "t20");
//                context.startActivity(intent);
            }
        });

        if(position%2==0)
        {
            holder.cardView.setCardBackgroundColor(ContextCompat.getColor(context,R.color.cback1));
            holder.txtplan.setBackground(ContextCompat.getDrawable(context,R.drawable.button_back_1));
        }
        else if(position%3==0)
        {
            holder.cardView.setCardBackgroundColor(ContextCompat.getColor(context,R.color.cback2));
            holder.txtplan.setBackground(ContextCompat.getDrawable(context,R.drawable.button_back_2));
        }
        else
        {
            holder.cardView.setCardBackgroundColor(ContextCompat.getColor(context,R.color.cback2));
            holder.txtplan.setBackground(ContextCompat.getDrawable(context,R.drawable.button_back_2));
        }
        Glide.with(context).load(current.team_1_logo).placeholder(R.mipmap.ic_launcher).into(holder.imgTeam1_logo);
        Glide.with(context).load(current.team_2_logo).placeholder(R.mipmap.ic_launcher).into(holder.imgTeam2_logo);

        holder.txtTeam1ShortName.setText(current.nickname_1);
        holder.txtTeam2ShortName.setText(current.nickname_2);
        holder.txtSeriesName.setText(current.series);


    }


    @Override
    public int getItemCount() {

        return data.size();
    }


    class MyViewHolder extends RecyclerView.ViewHolder {

        LinearLayout mainLayout;
        CircleImageView imgTeam1_logo, imgTeam2_logo;
        TextView txtTeam1Name, txtTeam2Name, txtTeam1ShortName, txtTeam2ShortName, txtRemainTime, txtSeriesName,txtplan;
        CardView cardView;

        public MyViewHolder(View itemView) {
            super(itemView);

            cardView= itemView.findViewById(R.id.cardView);
            mainLayout = itemView.findViewById(R.id.mainLayout);
            imgTeam1_logo = itemView.findViewById(R.id.imgTeam1_logo);
            imgTeam2_logo = itemView.findViewById(R.id.imgTeam2_logo);
            txtTeam1ShortName = itemView.findViewById(R.id.txtTeam1ShortName);
            txtTeam2ShortName = itemView.findViewById(R.id.txtTeam2ShortName);

            txtSeriesName = itemView.findViewById(R.id.txtSeriesName);
            txtRemainTime = itemView.findViewById(R.id.txtRemainTime);

            txtplan = itemView.findViewById(R.id.txtplan);


            // This code is used to get the screen dimensions of the user's device
            DisplayMetrics displayMetrics = new DisplayMetrics();
            activity.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
            int width = displayMetrics.widthPixels;
            int height = displayMetrics.heightPixels;

            Double f = Double.parseDouble(String.valueOf(width * 0.9));
            int i = Integer.valueOf(f.intValue());

            // Set the ViewHolder width to be a third of the screen size, and height to wrap content
            itemView.setLayoutParams(new RecyclerView.LayoutParams(i, RecyclerView.LayoutParams.WRAP_CONTENT));


        }


    }


}

