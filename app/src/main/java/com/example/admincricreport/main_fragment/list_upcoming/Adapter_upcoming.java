package com.example.admincricreport.main_fragment.list_upcoming;

import android.app.ProgressDialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.admincricreport.R;
import com.example.admincricreport.pojo.MatchesDetail;


import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class Adapter_upcoming extends RecyclerView.Adapter<Adapter_upcoming.MyViewHolder> {
    private LayoutInflater inflater;
    private static Context context;
    public Interface_upcoming interface_upcoming;

    List<Object> data = new ArrayList<>();

    private ProgressDialog dialog;

    public Adapter_upcoming(Context context, List<Object> data) {
        this.context = context;
        inflater = LayoutInflater.from(context);
        this.data = data;

    }

    public void onselectClick(Interface_upcoming interface_upcoming)
    {
        this.interface_upcoming=interface_upcoming;
    }

    @Override
    public Adapter_upcoming.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());

        View view = layoutInflater.inflate(R.layout.row_upcoming, parent, false);
        Adapter_upcoming.MyViewHolder viewHolder = new Adapter_upcoming.MyViewHolder(view);
        return viewHolder;


    }


    @Override
    public void onBindViewHolder(final Adapter_upcoming.MyViewHolder holder, int position) {

        final MatchesDetail current = (MatchesDetail) data.get(position);

        holder.mainLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Intent intent = new Intent(context, Plans.class);
//                intent.putExtra("mid", current.match_id);
//                intent.putExtra("m_type", "t20");
//                context.startActivity(intent);
                interface_upcoming.onClick(holder.getAdapterPosition());
            }
        });
        Glide.with(context).load(current.team_1_logo).placeholder(R.mipmap.ic_launcher).into(holder.imgTeam1_logo);
        Glide.with(context).load(current.team_2_logo).placeholder(R.mipmap.ic_launcher).into(holder.imgTeam2_logo);

        holder.txtTeam1ShortName.setText(current.nickname_1);
        holder.txtTeam2ShortName.setText(current.nickname_2);
        holder.txtSeriesName.setText(current.series);


//        callTime(holder, current.date_time);
    }

    public void callTime(Adapter_upcoming.MyViewHolder holder, String cdate) {

        // Custom date format
        SimpleDateFormat format = new SimpleDateFormat("d MMM, h:mm a");
        // Custom date format
        SimpleDateFormat format_d1 = new SimpleDateFormat("EEE MMM d HH:mm:ss");

        Date currentDate = new Date();
        String dateStop = cdate;
        Date d1 = null;
        Date d2 = null;
        try {
            d2 = format.parse(dateStop);
            d1 = format_d1.parse(String.valueOf(currentDate));
        } catch (ParseException e) {
            e.printStackTrace();
        }


        // Get msec from each, and subtract.
        long diff = d2.getTime() - d1.getTime();

        long diffSeconds = diff / 1000 % 60;
        long diffMinutes = diff / (60 * 1000) % 60;
        long diffHours = diff / (60 * 60 * 1000);
        long diffDay = diffHours / (24);
        long remainHour = diffHours % (24);
        String Hour = "", Min = "", Sec = "", Day = "";


        if (diffHours > 0) {
            if (remainHour > 0) {
                Hour = remainHour + "h";
            } else {
                Hour = diffHours + "h";
            }

        }
        if (diffMinutes > 0) {
            Min = diffMinutes + "m";
        }
        if (diffSeconds > 0) {
            Sec = diffSeconds + "s";
        }
        if (diffDay > 0) {
            Day = diffDay + "d";
        }
        holder.txtRemainTime.setText(Day + " " + Hour + " " + Min);

//        notifyDataSetChanged();

    }


    @Override
    public int getItemCount() {

        return data.size();
    }


    class MyViewHolder extends RecyclerView.ViewHolder {

        LinearLayout mainLayout;
        CircleImageView imgTeam1_logo, imgTeam2_logo;
        TextView txtTeam1Name, txtTeam2Name, txtTeam1ShortName, txtTeam2ShortName, txtRemainTime, txtSeriesName;

        public MyViewHolder(View itemView) {
            super(itemView);

            mainLayout = itemView.findViewById(R.id.mainLayout);
            imgTeam1_logo = itemView.findViewById(R.id.imgTeam1_logo);
            imgTeam2_logo = itemView.findViewById(R.id.imgTeam2_logo);
            txtTeam1ShortName = itemView.findViewById(R.id.txtTeam1ShortName);
            txtTeam2ShortName = itemView.findViewById(R.id.txtTeam2ShortName);

            txtSeriesName = itemView.findViewById(R.id.txtSeriesName);
            txtRemainTime = itemView.findViewById(R.id.txtRemainTime);


        }


    }


}

