package com.example.admincricreport.main_fragment.list_slider;

import android.app.ProgressDialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.admincricreport.R;


import java.util.ArrayList;
import java.util.List;

public class Adapter_slider extends RecyclerView.Adapter<Adapter_slider.MyViewHolder> {
    private LayoutInflater inflater;
    private static Context context;

    List<Information_slider> data = new ArrayList<>();

    private ProgressDialog dialog;

    public Adapter_slider(Context context, List<Information_slider> data) {
        this.context = context;
        inflater = LayoutInflater.from(context);
        this.data = data;

    }



    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());

            View view = layoutInflater.inflate(R.layout.row_slider, parent, false);
            MyViewHolder viewHolder = new MyViewHolder(view);
            return viewHolder;


    }


    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {

        Information_slider current =  data.get(position);

        Glide.with(context).load(R.drawable.slider).into(holder.imgSlider);

        holder.mainLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//                Intent intent=new Intent(context, SliderActivity.class);
//                context.startActivity(intent);

            }
        });

    }


    @Override
    public int getItemCount() {

        return data.size();
    }



    class MyViewHolder extends RecyclerView.ViewHolder {

        LinearLayout mainLayout;
        ImageView imgSlider;

        public MyViewHolder(View itemView) {
            super(itemView);

            imgSlider=itemView.findViewById(R.id.imgSlider);
            mainLayout=itemView.findViewById(R.id.mainLayout);


        }


    }


}

