package com.example.admincricreport.plan_pages.list_plan;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.RecyclerView;

import com.example.admincricreport.R;
import com.example.admincricreport.pojo.PlansDetail;

import java.util.ArrayList;
import java.util.List;

import me.grantland.widget.AutofitTextView;

public class Adapter_plans extends RecyclerView.Adapter<Adapter_plans.MyViewHolder> {
    private LayoutInflater inflater;
    private static Context context;
    private static Activity activity;
    private final ViewGroup nullParent = null;
    String match_key;
    public Interface_plans interface_plans;



    List<Object> data = new ArrayList<>();

    private ProgressDialog dialog;

    public Adapter_plans(Context context, List<Object> data,String match_key) {
        this.context = context;
        inflater = LayoutInflater.from(context);
        this.data = data;
        this.match_key=match_key;

    }

    public void onselectClick(Interface_plans interface_plans)
    {
        this.interface_plans=interface_plans;
    }


    @Override
    public Adapter_plans.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());

        View view = layoutInflater.inflate(R.layout.row_plans, parent, false);
        Adapter_plans.MyViewHolder viewHolder = new MyViewHolder(view);


        return viewHolder;


    }


    @Override
    public void onBindViewHolder(final Adapter_plans.MyViewHolder holder, final int position) {

        final PlansDetail current = (PlansDetail) data.get(position);

        if (current.listplan.size() > 0) {


            try {
                holder.layoutMega.removeAllViewsInLayout();

                LayoutInflater layoutInfralte = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

                for (int data = 0; data < current.listplan.size(); data++) {

                    View view = layoutInfralte.inflate(R.layout.row_plan_key_detail, nullParent, false);

                    AutofitTextView txtpricepool = view.findViewById(R.id.txtpricepool);
                    AutofitTextView txtspots = view.findViewById(R.id.txtspots);
                    AutofitTextView txtentryprice = view.findViewById(R.id.txtentryprice);
                    TextView txtfirstprice = view.findViewById(R.id.txtfirstprice);
                    txtpricepool.setText(context.getResources().getString(R.string.Rs) + current.listplan.get(data).poolprice);
                    txtentryprice.setText(context.getResources().getString(R.string.Rs) + current.listplan.get(data).entryprice);
                    txtfirstprice.setText("1st Winner " + context.getResources().getString(R.string.Rs) + current.listplan.get(data).firstprice);


                    final int finalData = data;
                    view.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {

                            interface_plans.onClick(finalData);

//                            Intent intent = new Intent(context, Contest_detail.class);
//                            intent.putExtra("key", current.listmega.get(finalData).key);
//                            intent.putExtra("keyPreContest", keyPreContest);
//                            intent.putExtra("type", "Mega Contest");
//                            intent.putExtra("mid", match_key);
//                            intent.putExtra("key_inning", key_inning);
//                            intent.putExtra("page", page);
//                            intent.putExtra("m_type", m_type);
//                            intent.addFlags(FLAG_ACTIVITY_NEW_TASK);
//                            context.startActivity(intent);
                        }
                    });

                    holder.layoutMega.addView(view);

                }

            } catch (Exception e1) {
                Toast.makeText(context, "" + e1.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }




    }


    @Override
    public int getItemCount() {

        return data.size();
    }


    class MyViewHolder extends RecyclerView.ViewHolder {

        LinearLayout mainLayout;
        TextView txtMegaTitle, txtMegaSubTitle;
        LinearLayout layoutMega, megamainLayout;

        public MyViewHolder(View itemView) {
            super(itemView);

            mainLayout = itemView.findViewById(R.id.mainLayout);
            layoutMega = itemView.findViewById(R.id.layoutMega);
            megamainLayout = itemView.findViewById(R.id.megamainLayout);


        }


    }


}

