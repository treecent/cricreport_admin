package com.example.admincricreport.plan_pages;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.admincricreport.R;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.HashMap;

public class Plans extends AppCompatActivity {

    Toolbar toolbar;
    String match_id;
    TextView txtprice;
    ProgressBar progressbar;
    EditText edtDetail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_plans);
        toolbar = findViewById(R.id.app_bar);

        txtprice = findViewById(R.id.txtprice);
        progressbar= findViewById(R.id.progressbar);
        edtDetail= findViewById(R.id.edtDetail);

        Intent i = getIntent();
        match_id = i.getStringExtra("match_id");

        if (match_id != null) {
            txtprice.setText("Selected Match Id : "+match_id);
        }

        getValue();

    }

    public void submitClick(View v) {

        if (edtDetail.getText().toString().isEmpty()) {
            Toast.makeText(this, "Enter Plan detail", Toast.LENGTH_SHORT).show();
        } else {
            progressbar.setVisibility(View.VISIBLE);

            DatabaseReference ref = FirebaseDatabase.getInstance().getReference("Plans_detail_predict/"+match_id);

            HashMap detail = new HashMap();
            detail.put("detail", edtDetail.getText().toString());

            ref.setValue(detail).addOnSuccessListener(new OnSuccessListener<Void>() {
                @Override
                public void onSuccess(Void aVoid) {
                    progressbar.setVisibility(View.GONE);
                    Toast.makeText(Plans.this, "Successfully Submited", Toast.LENGTH_SHORT).show();

                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    progressbar.setVisibility(View.GONE);
                    Toast.makeText(Plans.this, "Failed" + e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            });
        }
    }

    public void getValue()
    {
        DatabaseReference ref = FirebaseDatabase.getInstance().getReference("Plans_detail_predict/"+match_id);
        ref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {

                String detail="";
                if(!(snapshot.child("detail").getValue()==null)) {
                    detail = snapshot.child("detail").getValue().toString();
                }

                edtDetail.setText(detail);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {


            }
        });
    }


}