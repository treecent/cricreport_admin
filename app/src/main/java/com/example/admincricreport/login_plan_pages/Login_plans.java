package com.example.admincricreport.login_plan_pages;

import android.os.Bundle;

import com.example.admincricreport.plan_pages.list_plan.Adapter_plans;
import com.example.admincricreport.plan_pages.list_plan.Interface_plans;
import com.example.admincricreport.pojo.PlansDetail;
import com.example.admincricreport.pojo.PlansKeyDetail;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Looper;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.admincricreport.R;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.orhanobut.dialogplus.DialogPlus;
import com.orhanobut.dialogplus.OnDismissListener;
import com.orhanobut.dialogplus.OnItemClickListener;
import com.orhanobut.dialogplus.ViewHolder;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

public class Login_plans extends AppCompatActivity {

    ProgressBar progressbar_plan, progressbar, progressbar_update;
    private static List<PlansDetail> listPlans = new ArrayList<>();
    TextView txtNoPlan;
    RecyclerView recyclerView_plan;
    Adapter_plans adapter_plans;
    String match_id_hold = "";
    EditText edtplan,edtplandetail,edtplantype, edtplan_update,edtplan_update_desc,edtplan_update_type, edtlisting;
    CheckBox chkHide;
    String planKey = "";
    LinearLayout popupLayout;
    DialogPlus dialog = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_plans);

        progressbar_plan = findViewById(R.id.progressBar_plan);
        progressbar = findViewById(R.id.progressbar);
        progressbar_update = findViewById(R.id.progressbar_update);
        progressbar_plan.setVisibility(View.GONE);

        popupLayout = findViewById(R.id.popupLayout);
        edtplan = findViewById(R.id.edtplan);
        edtplandetail= findViewById(R.id.edtplandetail);
        edtplantype= findViewById(R.id.edtplantype);
        edtplan_update = findViewById(R.id.edtplan_update);
        edtplan_update_desc= findViewById(R.id.edtplan_update_desc);
        edtplan_update_type= findViewById(R.id.edtplan_update_type);
        edtlisting = findViewById(R.id.edtlisting);
        chkHide = findViewById(R.id.chkHide);
        recyclerView_plan = findViewById(R.id.recycler_plans);
        txtNoPlan = findViewById(R.id.txtNoPlan);

        load_plan();

    }

    public void load_plan() {
        try {
            LoadPlan loader = new LoadPlan();
            if (Looper.getMainLooper().getThread() == Thread.currentThread()) {
                Executor executor = Executors.newSingleThreadExecutor();
                executor.execute(loader);
                progressbar_plan.setVisibility(View.VISIBLE);
            }

        } catch (Exception ignored) {
        }

    }


    private class LoadPlan extends Thread {
        @Override
        public void run() {
            android.os.Process.setThreadPriority(android.os.Process.THREAD_PRIORITY_BACKGROUND);

            FirebaseDatabase database = FirebaseDatabase.getInstance();

            final DatabaseReference ref = database.getReference("/Plans");

            ref.orderByChild("listing").addValueEventListener(new com.google.firebase.database.ValueEventListener() {
                @Override
                public void onDataChange(@NonNull com.google.firebase.database.DataSnapshot snapshot) {


                    listPlans.clear();
                    final PlansDetail current = new PlansDetail();

                    for (final com.google.firebase.database.DataSnapshot dataSnapshot : snapshot.getChildren()) {
                        final PlansKeyDetail detail = new PlansKeyDetail();

//                        if ((dataSnapshot.child("status").getValue().equals("1"))) {

                        detail.key = String.valueOf(dataSnapshot.getKey());
                        detail.status = String.valueOf(dataSnapshot.child("status").getValue());
                        detail.poolprice = String.valueOf(dataSnapshot.child("poolprice").getValue());
                        detail.listing = String.valueOf(dataSnapshot.child("listing").getValue());
                        detail.desc = String.valueOf(dataSnapshot.child("desc").getValue());
                        detail.plantype = String.valueOf(dataSnapshot.child("plan").getValue());

                        current.listplan.add(detail);

//                        }

                    }


                    listPlans.add(current);
                    progressbar_plan.setVisibility(View.GONE);
                    if (listPlans != null) {
                        if (listPlans.size() > 0) {
                            callrecycle();
                        }
                    }
                    if (listPlans.get(0).listplan.size() > 0) {

                    } else {
                        txtNoPlan.setVisibility(View.VISIBLE);
                    }


                }

                @Override
                public void onCancelled(@NonNull DatabaseError error) {
                    progressbar_plan.setVisibility(View.GONE);
                    Toast.makeText(Login_plans.this, "" + error.getMessage(), Toast.LENGTH_SHORT).show();
                }
            });


        }
    }


    public void callrecycle() {
        adapter_plans = new Adapter_plans(Login_plans.this, getData(), match_id_hold);
        recyclerView_plan.setAdapter(adapter_plans);
        recyclerView_plan.setLayoutManager(new LinearLayoutManager(Login_plans.this, RecyclerView.VERTICAL, false));

        adapter_plans.onselectClick(new Interface_plans() {
            @Override
            public void onClick(int Position) {

                planKey = listPlans.get(0).listplan.get(Position).getKey();
                setValue(Position);
            }
        });
    }

    public List<Object> getData() {
        List<Object> data = new ArrayList<>();

        for (int i = 0; i < listPlans.size(); i++) {
            PlansDetail detail = new PlansDetail();

            detail = listPlans.get(i);

            data.add(detail);


        }
        return data;
    }

    public void submitClick(View v) {

        if (edtplan.getText().toString().isEmpty()) {
            Toast.makeText(this, "Enter Plan Price", Toast.LENGTH_SHORT).show();
        }
        else if (edtplandetail.getText().toString().isEmpty()) {
            Toast.makeText(this, "Enter Plan detail", Toast.LENGTH_SHORT).show();
        }
        else if (edtplantype.getText().toString().isEmpty()) {
            Toast.makeText(this, "Enter Plan Type", Toast.LENGTH_SHORT).show();
        }
        else {
            progressbar.setVisibility(View.VISIBLE);
            DatabaseReference ref = FirebaseDatabase.getInstance().getReference("Plans");
            HashMap detail = new HashMap();
            detail.put("poolprice", edtplan.getText().toString());
            detail.put("status", "1");
            detail.put("listing", "1");
            detail.put("desc", edtplandetail.getText().toString());
            detail.put("plan", edtplantype.getText().toString());
            ref.push().setValue(detail).addOnSuccessListener(new OnSuccessListener<Void>() {
                @Override
                public void onSuccess(Void aVoid) {
                    progressbar.setVisibility(View.GONE);
                    Toast.makeText(Login_plans.this, "Successfully Added", Toast.LENGTH_SHORT).show();
                    edtplan.setText("");
                    edtplandetail.setText("");
                    edtplantype.setText("");
                    load_plan();
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    progressbar.setVisibility(View.GONE);
                    Toast.makeText(Login_plans.this, "Failed" + e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            });
        }
    }

    public void updateClick(View v) {
        Toast.makeText(this, "" + planKey, Toast.LENGTH_SHORT).show();
        if (edtplan_update.getText().toString().isEmpty()) {
            Toast.makeText(this, "Enter Plan Price", Toast.LENGTH_SHORT).show();
        }else if (edtplan_update_desc.getText().toString().isEmpty()) {
            Toast.makeText(this, "Enter plan desc", Toast.LENGTH_SHORT).show();
        }
        else if (edtplan_update_type.getText().toString().isEmpty()) {
            Toast.makeText(this, "Enter plan type", Toast.LENGTH_SHORT).show();
        }
        else if (edtlisting.getText().toString().isEmpty()) {
            Toast.makeText(this, "Enter listing", Toast.LENGTH_SHORT).show();
        } else {
            String sts = "1";
            if (chkHide.isChecked()) {
                sts = "0";
            }
            int listing=Integer.parseInt(edtlisting.getText().toString());

            progressbar_update.setVisibility(View.VISIBLE);
            DatabaseReference ref = FirebaseDatabase.getInstance().getReference("Plans/" + planKey);
            HashMap detail = new HashMap();
            detail.put("poolprice", edtplan_update.getText().toString());
            detail.put("status", sts);
            detail.put("listing", listing);
            detail.put("desc", edtplan_update_desc.getText().toString());
            detail.put("plan", edtplan_update_type.getText().toString());
            ref.setValue(detail).addOnSuccessListener(new OnSuccessListener<Void>() {
                @Override
                public void onSuccess(Void aVoid) {
                    progressbar_update.setVisibility(View.GONE);
                    Toast.makeText(Login_plans.this, "Successfully Updated", Toast.LENGTH_SHORT).show();
                    edtplan.setText("");
                    dialog.dismiss();
                    load_plan();
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    progressbar_update.setVisibility(View.GONE);
                    dialog.dismiss();
                    Toast.makeText(Login_plans.this, "Failed" + e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            });
        }

    }

    public void showPopup() {

        popupLayout.setVisibility(View.VISIBLE);

        dialog = DialogPlus.newDialog(Login_plans.this)


                .setContentHolder(new ViewHolder(popupLayout))
                .setOnItemClickListener(new OnItemClickListener() {
                    @Override
                    public void onItemClick(DialogPlus dialog, Object item, View view, int position) {
                    }
                })
                .setExpanded(true, ViewGroup.LayoutParams.WRAP_CONTENT)
                .setGravity(Gravity.BOTTOM)
                .setMargin(0, 500, 0, 0)
//                .setInAnimation(R.anim.abc_fade_in)
//                .setOutAnimation(R.anim.abc_fade_out)


                .setContentBackgroundResource(R.color.colorPrimary_faint)// This will enable the expand feature, (similar to android L share dialog)
                .setCancelable(true)

                .setOnDismissListener(new OnDismissListener() {
                    @Override
                    public void onDismiss(DialogPlus dialogPlus) {
                        popupLayout.setVisibility(View.GONE);
                    }
                })


                .create();

        dialog.show();


    }

    public void setValue(int position) {
        edtplan_update.setText(listPlans.get(0).listplan.get(position).getPoolprice());
        edtplan_update_desc.setText(listPlans.get(0).listplan.get(position).getDesc());
        edtplan_update_type.setText(listPlans.get(0).listplan.get(position).getPlantype());
        edtlisting.setText(listPlans.get(0).listplan.get(position).getListing());
        showPopup();
    }

}